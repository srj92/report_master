package co.md.converter;

import java.io.IOException;
import java.util.List;

import javax.persistence.AttributeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.md.model.DbConnection;

public class JpaDbConnConverter implements AttributeConverter<List<DbConnection>, String> {

	private static final Logger LOG = LoggerFactory.getLogger(JpaDbConnConverter.class);
	private final static ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(List<DbConnection> dbConnections) {

		try {
			return objectMapper.writeValueAsString(dbConnections);
		} catch (JsonProcessingException ex) {
			LOG.error("Unexpected JsonProcessingException writing dbConnections to database: "
					+ dbConnections.toString());
			return null;
		}

	}

	@Override
	public List<DbConnection> convertToEntityAttribute(String dbConnJson) {
		try {
			return objectMapper.readValue(dbConnJson, new TypeReference<List<DbConnection>>(){});
		} catch (IOException ex) {
			LOG.error("Unexpected IOEx decoding json from database: " + dbConnJson);
			return null;
		}
	}

}
