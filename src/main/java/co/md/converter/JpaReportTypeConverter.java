package co.md.converter;

import java.io.IOException;
import java.util.List;

import javax.persistence.AttributeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.md.model.Report;

public class JpaReportTypeConverter implements AttributeConverter<List<Report>, String> {

	private static final Logger LOG = LoggerFactory.getLogger(JpaReportTypeConverter.class);
	private final static ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(List<Report> reportTypes) {
		try {
			return objectMapper.writeValueAsString(reportTypes);
		} catch (JsonProcessingException ex) {
			LOG.error("Unexpected JsonProcessingException writing reportTypes to database: " + reportTypes.toString());
			return null;
		}
	}

	@Override
	public List<Report> convertToEntityAttribute(String reportJson) {
		try {
			return objectMapper.readValue(reportJson, new TypeReference<List<Report>>(){});
		} catch (IOException ex) {
			LOG.error(""+ex.toString());
			LOG.error("Unexpected IOEx decoding json from database: " + reportJson);
			return null;
		}
	}

}
