package co.md.exception;

import co.md.enums.ErrorCodeEnum;

public class ReportMasterException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private String message;
	private ErrorCodeEnum errorCode;
	
	public ReportMasterException(Throwable cause) {
		super(cause);
	}
	
	public ReportMasterException(String message) {
		super();
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ErrorCodeEnum getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCodeEnum errorCode) {
		this.errorCode = errorCode;
	}
	
}
