package co.md.handler;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.md.enums.ReportStateEnum;
import co.md.exception.ClientException;
import co.md.exception.ReportMasterException;
import co.md.model.ReportStateHistory;
import co.md.service.ReportStateHistoryService;
import co.md.util.DateTimeUtils;

@Component
public class ReportExceptionHandler extends BaseExceptionHandler {

	@Autowired
	private ReportStateHistoryService reportStateHistoryService;
	
	public void updateReportStatus(Throwable cause) {
		
		if (cause instanceof ReportMasterException) {
			ReportStateHistory reportStateHistory = new ReportStateHistory();
			if (cause instanceof ClientException) {
				ClientException clientException = (ClientException) cause;
				super.logException(clientException);
				Date currentDate = DateTimeUtils.getCurrentDate();
				if (clientException.getPayload() != null) {
					reportStateHistory.setCampaignId(clientException.getPayload().getCampaignId());
					reportStateHistory.setReportType(clientException.getPayload().getReport().getReportName());
				}
				reportStateHistory.setCreationDate(currentDate);
				reportStateHistory.setErrorCode(clientException.getErrorCode().getCode());
				reportStateHistory.setErrorMessage(clientException.getMessage());
				reportStateHistory.setModifyDate(currentDate);
			}
			else {
				// TODO
			}
			reportStateHistory.setState(ReportStateEnum.REPORT_MASTER_FAIL.getReportState());
			reportStateHistoryService.updateStateHistory(reportStateHistory);
		}
	}

}
