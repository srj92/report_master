package co.md.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseExceptionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(BaseExceptionHandler.class);
	
	protected void logException(Throwable throwable) {
		LOG.error("-----> Exception occured: " + throwable.getCause());
	}
}
