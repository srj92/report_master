package co.md.bean;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.md.enums.ReportTypeEnum;
import co.md.model.DbConnection;

public class ReportRequest {

	private Long campaignId;
	private ReportTypeEnum report;
	private List<DbConnection> dbConnections;
	private String startDate;
	private String endDate;

	public ReportRequest() {
		super();
	}

	public Long getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}

	public ReportTypeEnum getReport() {
		return report;
	}

	public void setReport(ReportTypeEnum report) {
		this.report = report;
	}

	public List<DbConnection> getDbConnections() {
		return dbConnections;
	}

	public void setDbConnections(List<DbConnection> dbs) {
		this.dbConnections = dbs;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public static ReportRequest fromJson(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ReportRequest request = mapper.readValue(json, ReportRequest.class);
		return request;
	}
	
	public static String toJson(ReportRequest reportRequest) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(reportRequest);
		return json;
	}

	@Override
	public String toString() {
		return "ReportRequest [campaignId=" + campaignId + ", report=" + report + ", dbConnections=" + dbConnections
				+ ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}