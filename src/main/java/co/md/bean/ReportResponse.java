package co.md.bean;

public class ReportResponse {

	private Integer campaignId;
	private String report;
	private String message;
	
	public ReportResponse() {
		super();
	}
	
	public Integer getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "ReportResponse [campaignId=" + campaignId + ", report=" + report + ", message=" + message + "]";
	}
	
	
	
}
