package co.md.bean;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class ReportMasterRequest {

	public ReportMasterRequest() {}
	
	public ReportMasterRequest(String date) {
		super();
		this.date = date;
	}

	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public static ReportMasterRequest fromJson(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ReportMasterRequest request = mapper.readValue(json, ReportMasterRequest.class);
		return request;
	}
	
	public static String toJson(ReportMasterRequest reportRequest) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(reportRequest);
		return json;
	}

	@Override
	public String toString() {
		return "ReportMasterRequest [date=" + date + "]";
	}
	
}
