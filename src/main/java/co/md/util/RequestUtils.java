package co.md.util;

import static co.md.util.DateTimeUtils.getDateAsString;

import java.util.Date;

import co.md.bean.ReportRequest;
import co.md.model.CmsCampaign;

public class RequestUtils {

	public static ReportRequest generateReportRequest(Date startDate, Date endDate, CmsCampaign c) {
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setStartDate(getDateAsString(startDate));
		reportRequest.setEndDate(getDateAsString(endDate));
		if (c != null) {
			reportRequest.setCampaignId(c.getCmpId());
			reportRequest.setDbConnections(c.getDbConnections());
		}
		return reportRequest;
	}
}