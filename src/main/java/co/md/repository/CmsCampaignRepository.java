package co.md.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.md.model.CmsCampaign;

@Repository
public interface CmsCampaignRepository extends CrudRepository<CmsCampaign, Integer>{

	@Query("SELECT c FROM CmsCampaign c WHERE c.endDate > ?1")
	List<CmsCampaign> findAllValid(Date date);
}
