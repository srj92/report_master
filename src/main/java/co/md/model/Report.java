package co.md.model;

public class Report {

	private Integer id;
	private String name;
	
	public Report(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Report() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
