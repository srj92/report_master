package co.md.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import co.md.converter.JpaDateConverter;
import co.md.converter.JpaDbConnConverter;
import co.md.converter.JpaReportTypeConverter;

@Entity
@Table(name = "cms_campaign")
public class CmsCampaign {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "userid")
	private Long userId;

	@Column(name = "advid")
	private Long advId;

	@Column(name = "cmp_id")
	private Long cmpId;

	private String name;

	private String category;

	@Column(name = "start_date", columnDefinition = "DATETIME")
	@Convert(converter = JpaDateConverter.class)
	private Date startDate;

	@Column(name = "end_date", columnDefinition = "DATETIME")
	@Convert(converter = JpaDateConverter.class)
	private Date endDate;

	private Float budget;

	private String objective;

	@Column(name = "billing_model")
	private Long billingModel;

	private Float revenue;

	@Column(name = "ext_cmp_id")
	private Long extCmpId;

	@Column(name = "ext_name")
	private String extName;

	@Column(columnDefinition = "TEXT")
	private String params;

	private Long status;

	@Column(name = "report_type")
	@Convert(converter = JpaReportTypeConverter.class)
	private List<Report> reports;

	@Column(name = "db_conn")
	@Convert(converter = JpaDbConnConverter.class)
	private List<DbConnection> dbConnections;

	@Column(name = "created_date", columnDefinition = "DATETIME")
	@Convert(converter = JpaDateConverter.class)
	private Date createdDate;

	@Column(name = "modified_date", columnDefinition = "DATETIME")
	@Convert(converter = JpaDateConverter.class)
	private Date modifiedDate;

	public CmsCampaign() {
		super();
	}

	public Long getId() {
		return id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getAdvId() {
		return advId;
	}

	public void setAdvId(Long advId) {
		this.advId = advId;
	}

	public Long getCmpId() {
		return cmpId;
	}

	public void setCmpId(Long cmpId) {
		this.cmpId = cmpId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Float getBudget() {
		return budget;
	}

	public void setBudget(Float budget) {
		this.budget = budget;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public Long getBillingModel() {
		return billingModel;
	}

	public void setBillingModel(Long billingModel) {
		this.billingModel = billingModel;
	}

	public Float getRevenue() {
		return revenue;
	}

	public void setRevenue(Float revenue) {
		this.revenue = revenue;
	}

	public Long getExtCmpId() {
		return extCmpId;
	}

	public void setExtCmpId(Long extCmpId) {
		this.extCmpId = extCmpId;
	}

	public String getExtName() {
		return extName;
	}

	public void setExtName(String extName) {
		this.extName = extName;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public List<Report> getReports() {
		return reports;
	}

	public void setReports(List<Report> reports) {
		this.reports = reports;
	}
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public List<DbConnection> getDbConnections() {
		return dbConnections;
	}

	public void setDbConnections(List<DbConnection> dbConnections) {
		this.dbConnections = dbConnections;
	}
	
	

	@Override
	public String toString() {
		return "CmsCampaign [id=" + id + ", userId=" + userId + ", advId=" + advId + ", cmpId=" + cmpId + ", name="
				+ name + ", category=" + category + ", startDate=" + startDate + ", endDate=" + endDate + ", budget="
				+ budget + ", objective=" + objective + ", billingModel=" + billingModel + ", revenue=" + revenue
				+ ", extCmpId=" + extCmpId + ", extName=" + extName + ", params=" + params + ", status=" + status
				+ ", reports=" + reports + ", dbConnections=" + dbConnections + ", createdDate=" + createdDate
				+ ", modifiedDate=" + modifiedDate + "]";
	}

}
