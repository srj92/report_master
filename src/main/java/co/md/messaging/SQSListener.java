package co.md.messaging;

import static co.md.util.DateTimeUtils.getDateFromString;

import java.io.IOException;
import java.text.ParseException;

import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import co.md.bean.ReportMasterRequest;
import co.md.service.AnalyticsService;

@Service
public class SQSListener {

	private static final Logger LOG = LoggerFactory.getLogger(SQSListener.class);
	private static final String INPUT_QUEUE = "report_master_queue";
	
	@Autowired
	private AnalyticsService analyticsSQSService;
	
	@JmsListener(destination = INPUT_QUEUE)
	public void onMessage(String requestJson) throws JMSException, ParseException {
		LOG.info("Received message: {}", requestJson);
		try {
			ReportMasterRequest reportMasterRequest = ReportMasterRequest.fromJson(requestJson);
			analyticsSQSService.generateAnalytics(getDateFromString(reportMasterRequest.getDate()), true, getDateFromString(reportMasterRequest.getDate()), getDateFromString(reportMasterRequest.getDate()));
		} catch (IOException e) {
			LOG.error("Encountered error while parsing message.", e);
			throw new JMSException("Encountered error while parsing message.");
		}
	}
	
}
