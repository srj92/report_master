package co.md.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.md.model.CmsCampaign;
import co.md.repository.CmsCampaignRepository;

@Service
@Transactional
public class CmsCampaignServiceImpl implements CmsCampaignService {

	private static final Logger LOG = LoggerFactory.getLogger(CmsCampaignServiceImpl.class);
	
	@Autowired
	private CmsCampaignRepository cmsCampaignRepository;
	
	@Override
	public List<CmsCampaign> findAllValidCampaigns(Date date) {
		LOG.info("Finding all valid campaigns for date: {}", date.toString());
		return cmsCampaignRepository.findAllValid(date);
	}

}
