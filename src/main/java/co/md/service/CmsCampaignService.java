package co.md.service;

import java.util.Date;
import java.util.List;

import co.md.model.CmsCampaign;

public interface CmsCampaignService {

	List<CmsCampaign> findAllValidCampaigns(Date date);
}
