package co.md.service;

import static co.md.enums.ReportStateEnum.REPORT_MASTER_SUCCESS;
import static co.md.enums.ReportTypeEnum.BROWSER;
import static co.md.enums.ReportTypeEnum.CREATIVE;
import static co.md.enums.ReportTypeEnum.DEVICE;
import static co.md.enums.ReportTypeEnum.GEO;
import static co.md.enums.ReportTypeEnum.OS;
import static co.md.enums.ReportTypeEnum.OVERVIEW;
import static co.md.enums.ReportTypeEnum.PUBLISHER;
import static co.md.enums.ReportTypeEnum.SOURCE;
import static co.md.enums.ReportTypeEnum.TIMEBAND;
import static co.md.util.RequestUtils.generateReportRequest;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import co.md.bean.ReportRequest;
import co.md.client.ReportGenerationClient;
import co.md.exception.ClientException;
import co.md.handler.ReportExceptionHandler;
import co.md.model.CmsCampaign;
import co.md.model.ReportStateHistory;
import co.md.util.DateTimeUtils;

@Service
@Qualifier("analyticsSQSService")
public class AnalyticsSQSService extends AnalyticsService {

	private static final Logger LOG = LoggerFactory.getLogger(AnalyticsSQSService.class);

	@Autowired
	private CmsCampaignService cmsCampaignService;

	@Autowired
	@Qualifier("reportGenerationSQSClient")
	private ReportGenerationClient reportGenerationSQSClient;

	@Autowired
	private ReportStateHistoryService reportStateHistoryService;
	@Autowired
	private ReportExceptionHandler reportExceptionHandler;
	
	@Override
	protected void generateReports(Date startDate, Date endDate) {
		ReportRequest reportRequest = generateReportRequest(startDate, endDate, null);
		try {
			reportGenerationSQSClient.generateBrowserReport(reportRequest);
			reportGenerationSQSClient.generateCreativeReport(reportRequest);
			reportGenerationSQSClient.generateDeviceReport(reportRequest);
			reportGenerationSQSClient.generateGeoReport(reportRequest);
			reportGenerationSQSClient.generateOSReport(reportRequest);
			reportGenerationSQSClient.generateOverviewReport(reportRequest);
			reportGenerationSQSClient.generatePublisherReport(reportRequest);
			reportGenerationSQSClient.generateSourceReport(reportRequest);
			reportGenerationSQSClient.generateTimebandReport(reportRequest);			
		}
		catch (RuntimeException e) {
			LOG.error("Exception occured: {}",e);
			reportExceptionHandler.updateReportStatus(e.getCause());
		}
	}

	@Override
	public void generateReports(List<CmsCampaign> validCampaigns, Date startDate, Date endDate) {

		LOG.info("Calling report generation services for each campaign. Number of campaigns: {}",
				validCampaigns.size());
		validCampaigns.forEach((c) -> {

			LOG.info("Calling report generation client for campaignId: {}", c.getCmpId());
			if (c.getReports() != null && !c.getReports().isEmpty()) {
				ReportRequest reportRequest = generateReportRequest(startDate, endDate, c);
				if (checkReportType(c, BROWSER.getReportName())) {
					try {
						reportGenerationSQSClient.generateBrowserReport(reportRequest);
						updateSuccessReportHistory(reportRequest);
					} catch (ClientException e) {
						reportExceptionHandler.updateReportStatus(e);
					}
				}
				if (checkReportType(c, CREATIVE.getReportName())) {
					try {
						reportGenerationSQSClient.generateCreativeReport(reportRequest);
						updateSuccessReportHistory(reportRequest);
					} catch (ClientException e) {
						reportExceptionHandler.updateReportStatus(e);
					}
				}
				if (checkReportType(c, DEVICE.getReportName())) {
					try {
						reportGenerationSQSClient.generateDeviceReport(reportRequest);
						updateSuccessReportHistory(reportRequest);
					} catch (ClientException e) {
						reportExceptionHandler.updateReportStatus(e);
					}
				}
				if (checkReportType(c, GEO.getReportName())) {
					try {
						reportGenerationSQSClient.generateGeoReport(reportRequest);
						updateSuccessReportHistory(reportRequest);
					} catch (ClientException e) {
						reportExceptionHandler.updateReportStatus(e);
					}

				}
				if (checkReportType(c, OS.getReportName())) {
					try {
						reportGenerationSQSClient.generateOSReport(reportRequest);
						updateSuccessReportHistory(reportRequest);
					} catch (ClientException e) {
						reportExceptionHandler.updateReportStatus(e);
					}

				}
				if (checkReportType(c, OVERVIEW.getReportName())) {
					try {
						reportGenerationSQSClient.generateOverviewReport(reportRequest);
						updateSuccessReportHistory(reportRequest);
					} catch (ClientException e) {
						reportExceptionHandler.updateReportStatus(e);
					}

				}
				if (checkReportType(c, PUBLISHER.getReportName())) {
					try {
						reportGenerationSQSClient.generatePublisherReport(reportRequest);
						updateSuccessReportHistory(reportRequest);
					} catch (ClientException e) {
						reportExceptionHandler.updateReportStatus(e);
					}

				}
				if (checkReportType(c, SOURCE.getReportName())) {
					try {
						reportGenerationSQSClient.generateSourceReport(reportRequest);
						updateSuccessReportHistory(reportRequest);
					} catch (ClientException e) {
						reportExceptionHandler.updateReportStatus(e);
					}

				}
				if (checkReportType(c, TIMEBAND.getReportName())) {
					try {
						reportGenerationSQSClient.generateTimebandReport(reportRequest);
						updateSuccessReportHistory(reportRequest);
					} catch (ClientException e) {
						reportExceptionHandler.updateReportStatus(e);
					}
				}
			}
		});
	}

	private boolean checkReportType(CmsCampaign c, String reportName) {
		return c.getReports().stream().anyMatch(r -> r.getName().equalsIgnoreCase(reportName));
	}

	private void updateSuccessReportHistory(ReportRequest request) {
		Date currentDate = DateTimeUtils.getCurrentDate();
		ReportStateHistory reportStateHistory = new ReportStateHistory();
		if (request != null) {
			reportStateHistory.setCampaignId(request.getCampaignId());
			reportStateHistory.setReportType(request.getReport().getReportName());
		}
		reportStateHistory.setCreationDate(currentDate);
		reportStateHistory.setErrorCode(null);
		reportStateHistory.setErrorMessage(null);
		reportStateHistory.setModifyDate(currentDate);
		
		reportStateHistory.setState(REPORT_MASTER_SUCCESS.getReportState());
		reportStateHistoryService.updateStateHistory(reportStateHistory);
	}


	@Override
	public List<CmsCampaign> findAllValidCampaigns(Date date) {
		LOG.info("Getting all valid campaign and report data from db");
		List<CmsCampaign> validCampaigns = cmsCampaignService.findAllValidCampaigns(date);
		return validCampaigns;
	}

	@Override
	protected void updateApplicationStateHistory() {
		LOG.info("Application duty finished. Updating state");
		this.updateSuccessReportHistory(null);
	}

}
