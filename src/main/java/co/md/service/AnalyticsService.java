package co.md.service;

import java.util.Date;
import java.util.List;

import co.md.model.CmsCampaign;

public abstract class AnalyticsService {

	public void generateAnalytics(Date endDateToCheckForValidCmp, boolean allCampaigns, Date startDate, Date endDate) {
		if (!allCampaigns) {
			List<CmsCampaign> validCampaigns = findAllValidCampaigns(endDateToCheckForValidCmp);
			generateReports(validCampaigns, startDate, endDate);
		}
		else {
			generateReports(startDate, endDate);	
		}
		updateApplicationStateHistory();
	}

	protected abstract List<CmsCampaign> findAllValidCampaigns(Date date);

	protected abstract void generateReports(List<CmsCampaign> campaigns, Date startDate, Date endDate);

	protected abstract void generateReports(Date startDate, Date endDate);
	
	protected abstract void updateApplicationStateHistory();

}
