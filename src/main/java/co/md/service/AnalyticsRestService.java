/*package co.md.service;

import static co.md.enums.ReportTypeEnum.BROWSER;
import static co.md.enums.ReportTypeEnum.CREATIVE;
import static co.md.enums.ReportTypeEnum.DEVICE;
import static co.md.enums.ReportTypeEnum.GEO;
import static co.md.enums.ReportTypeEnum.OS;
import static co.md.enums.ReportTypeEnum.OVERVIEW;
import static co.md.enums.ReportTypeEnum.PUBLISHER;
import static co.md.enums.ReportTypeEnum.SOURCE;
import static co.md.enums.ReportTypeEnum.TIMEBAND;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import co.md.bean.ReportRequest;
import co.md.client.ReportGenerationClient;
import co.md.enums.ReportStateEnum;
import co.md.enums.ReportTypeEnum;
import co.md.model.CmsCampaign;
import co.md.model.ReportStateHistory;
import co.md.util.DateTimeUtils;

@Service
@Qualifier("analyticsRestService")
public class AnalyticsRestService extends AnalyticsService {

	private static final Logger LOG = LoggerFactory.getLogger(AnalyticsRestService.class);

	@Autowired
	private CmsCampaignService cmsCampaignService;
	@Autowired
	private ReportGenerationClient reportGenerationRestClient;
	@Autowired
	private ReportStateHistoryService reportStateHistoryService;

	@Override
	protected void generateReports(List<CmsCampaign> validCampaigns, Date startDate, Date endDate) {

		LOG.info("Calling report generation services for each campaign. Number of campaigns: {}",
				validCampaigns.size());
		validCampaigns.forEach((c) -> {

			LOG.info("Calling report generation client for campaignId: {}", c.getCmpId());

			if (c.getReports() != null && !c.getReports().isEmpty()) {
				if (c.getReports().stream().anyMatch(r -> r.getName().equalsIgnoreCase(BROWSER.getReportName()))) {
					ReportRequest reportRequest = generateReportRequest(c, BROWSER);
					reportGenerationRestClient.generateBrowserReport(reportRequest);
				}
				if (c.getReports().stream().anyMatch(r -> r.getName().equalsIgnoreCase(CREATIVE.getReportName()))) {
					ReportRequest reportRequest = generateReportRequest(c, CREATIVE);
					reportGenerationRestClient.generateCreativeReport(reportRequest);
				}
				if (c.getReports().stream().anyMatch(r -> r.getName().equalsIgnoreCase(DEVICE.getReportName()))) {
					ReportRequest reportRequest = generateReportRequest(c, DEVICE);
					reportGenerationRestClient.generateDeviceReport(reportRequest);
				}
				if (c.getReports().stream().anyMatch(r -> r.getName().equalsIgnoreCase(GEO.getReportName()))) {
					ReportRequest reportRequest = generateReportRequest(c, GEO);
					reportGenerationRestClient.generateGeoReport(reportRequest);
				}
				if (c.getReports().stream().anyMatch(r -> r.getName().equalsIgnoreCase(OS.getReportName()))) {
					ReportRequest reportRequest = generateReportRequest(c, OS);
					reportGenerationRestClient.generateOSReport(reportRequest);
				}
				if (c.getReports().stream().anyMatch(r -> r.getName().equalsIgnoreCase(OVERVIEW.getReportName()))) {
					ReportRequest reportRequest = generateReportRequest(c, OVERVIEW);
					reportGenerationRestClient.generateOverviewReport(reportRequest);
				}
				if (c.getReports().stream().anyMatch(r -> r.getName().equalsIgnoreCase(PUBLISHER.getReportName()))) {
					ReportRequest reportRequest = generateReportRequest(c, PUBLISHER);
					reportGenerationRestClient.generatePublisherReport(reportRequest);
				}
				if (c.getReports().stream().anyMatch(r -> r.getName().equalsIgnoreCase(SOURCE.getReportName()))) {
					ReportRequest reportRequest = generateReportRequest(c, SOURCE);
					reportGenerationRestClient.generateSourceReport(reportRequest);
				}
				if (c.getReports().stream().anyMatch(r -> r.getName().equalsIgnoreCase(TIMEBAND.getReportName()))) {
					ReportRequest reportRequest = generateReportRequest(c, TIMEBAND);
					reportGenerationRestClient.generateTimebandReport(reportRequest);
				}
			}
		});

		LOG.info("All async calls made. Handling exceptions if any");
	}

	private void updateSuccessReportHistory(ReportRequest request) {
		Date currentDate = DateTimeUtils.getCurrentDate();
		ReportStateHistory reportStateHistory = new ReportStateHistory();
		reportStateHistory.setCampaignId(request.getCampaignId());
		reportStateHistory.setCreationDate(currentDate);
		reportStateHistory.setErrorCode(null);
		reportStateHistory.setErrorMessage(null);
		reportStateHistory.setModifyDate(currentDate);
		reportStateHistory.setReportType(request.getReport().getReportName());
		reportStateHistory.setState(ReportStateEnum.REPORT_MASTER_SUCCESS.getReportState());
		reportStateHistoryService.updateReportStatus(reportStateHistory);
		LOG.info(request.toString());
	}

	private ReportRequest generateReportRequest(CmsCampaign c, ReportTypeEnum reportEnum) {
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setStartDate(DateTimeUtils.getDateAsString(DateTimeUtils.getCurrentDate()));
		reportRequest.setEndDate(DateTimeUtils.getDateAsString(DateTimeUtils.getCurrentDate()));
		reportRequest.setCampaignId(c.getCmpId());
		reportRequest.setReport(reportEnum);
		reportRequest.setDbConnections(c.getDbConnections());
		return reportRequest;
	}

	@Override
	public List<CmsCampaign> findAllValidCampaigns(Date date) {
		LOG.info("Getting all valid campaign and report data from db");
		List<CmsCampaign> validCampaigns = cmsCampaignService.findAllValidCampaigns(date);
		return validCampaigns;
	}

	@Override
	protected void generateReports(Date startDate, Date endDate) {
		// TODO 

	}

}
*/