/*package co.md.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import co.md.bean.ReportRequest;
import co.md.bean.ReportResponse;
import co.md.exception.ClientException;

@Async
@Component
@Qualifier("reportGenerationSQSClient")
public class ReportGenerationRestClient extends ReportGenerationClient {

	private static final Logger LOG = LoggerFactory.getLogger(ReportGenerationRestClient.class);

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public void generateOverviewReport(ReportRequest reportRequest) {
		try {
			LOG.info("Calling web service: " + reportRequest.getReport().getReportName());
			restTemplate.postForEntity("", reportRequest, ReportResponse.class);
		} catch (RuntimeException e) {
			LOG.error(reportRequest.getReport().getReportName() + " report service call failed for campaignId: "
					+ reportRequest.getCampaignId());
			LOG.error("-----> " + e);
			throw new ClientException(reportRequest, e);
		}
	}

	@Override
	public void generatePublisherReport(ReportRequest reportRequest) {
		try {
			LOG.info("Calling web service: " + reportRequest.getReport().getReportName());
			restTemplate.postForEntity("", reportRequest, ReportResponse.class);
		} catch (RuntimeException e) {
			LOG.error(reportRequest.getReport().getReportName() + " report service call failed for campaignId: "
					+ reportRequest.getCampaignId());
			LOG.error("-----> " + e);
			throw new ClientException(reportRequest, e);
		}
	}

	@Override
	public void generateCreativeReport(ReportRequest reportRequest) {
		try {
			LOG.info("Calling web service: " + reportRequest.getReport().getReportName());
			restTemplate.postForEntity("", reportRequest, ReportResponse.class);
		} catch (RuntimeException e) {
			LOG.error(reportRequest.getReport().getReportName() + " report service call failed for campaignId: "
					+ reportRequest.getCampaignId());
			LOG.error("-----> " + e);
			throw new ClientException(reportRequest, e);
		}
	}

	@Override
	public void generateGeoReport(ReportRequest reportRequest) {
		try {
			LOG.info("Calling web service: " + reportRequest.getReport().getReportName());
			restTemplate.postForEntity("", reportRequest, ReportResponse.class);
		} catch (RuntimeException e) {
			LOG.error(reportRequest.getReport().getReportName() + " report service call failed for campaignId: "
					+ reportRequest.getCampaignId());
			LOG.error("-----> " + e);
			throw new ClientException(reportRequest, e);
		}
	}

	@Override
	public void generateDeviceReport(ReportRequest reportRequest) {
		try {
			LOG.info("Calling web service: " + reportRequest.getReport().getReportName());
			restTemplate.postForEntity("", reportRequest, ReportResponse.class);
		} catch (RuntimeException e) {
			LOG.error(reportRequest.getReport().getReportName() + " report service call failed for campaignId: "
					+ reportRequest.getCampaignId());
			LOG.error("-----> " + e);
			throw new ClientException(reportRequest, e);
		}
	}

	@Override
	public void generateOSReport(ReportRequest reportRequest) {
		try {
			LOG.info("Calling web service: " + reportRequest.getReport().getReportName());
			restTemplate.postForEntity("", reportRequest, ReportResponse.class);
		} catch (RuntimeException e) {
			LOG.error(reportRequest.getReport().getReportName() + " report service call failed for campaignId: "
					+ reportRequest.getCampaignId());
			LOG.error("-----> " + e);
			throw new ClientException(reportRequest, e);
		}
	}

	@Override
	public void generateBrowserReport(ReportRequest reportRequest) {
		try {
			LOG.info("Calling web service: " + reportRequest.getReport().getReportName());
			restTemplate.postForEntity("", reportRequest, ReportResponse.class);
		} catch (RuntimeException e) {
			LOG.error(reportRequest.getReport().getReportName() + " report service call failed for campaignId: "
					+ reportRequest.getCampaignId());
			LOG.error("-----> " + e);
			throw new ClientException(reportRequest, e);
		}
	}

	@Override
	public void generateTimebandReport(ReportRequest reportRequest) {
		try {
			LOG.info("Calling web service: " + reportRequest.getReport().getReportName());
			restTemplate.postForEntity("", reportRequest, ReportResponse.class);
		} catch (RuntimeException e) {
			LOG.error(reportRequest.getReport().getReportName() + " report service call failed for campaignId: "
					+ reportRequest.getCampaignId());
			LOG.error("-----> " + e);
			throw new ClientException(reportRequest, e);
		}
	}

	@Override
	public void generateSourceReport(ReportRequest reportRequest) {
		try {
			LOG.info("Calling web service: " + reportRequest.getReport().getReportName());
			restTemplate.postForEntity("", reportRequest, ReportResponse.class);
		} catch (RuntimeException e) {
			LOG.error(reportRequest.getReport().getReportName() + " report service call failed for campaignId: "
					+ reportRequest.getCampaignId());
			LOG.error("-----> " + e);
			throw new ClientException(reportRequest, e);
		}
	}
}*/