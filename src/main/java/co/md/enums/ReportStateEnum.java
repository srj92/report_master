package co.md.enums;

public enum ReportStateEnum {

	REPORT_MASTER_FAIL(20l),
	REPORT_MASTER_SUCCESS(21l);

	private Long reportState;
	
	private ReportStateEnum(Long reportState) {
		this.reportState = reportState;
	}

	public Long getReportState() {
		return reportState;
	}

	public void setReportState(Long reportState) {
		this.reportState = reportState;
	}

}
