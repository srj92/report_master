package co.md.service;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import co.md.ReportMasterAppTests;
import co.md.util.DateTimeUtils;

@Ignore
public class AnalyticsServiceTest extends ReportMasterAppTests {

	@Autowired
	@Qualifier("analyticsSQSService")
	private AnalyticsService analyticsService;
	
	@Test
	public void testAnalyticsService() {
		analyticsService.generateAnalytics(DateTimeUtils.getCurrentDate(), true, DateTimeUtils.getCurrentDate(), DateTimeUtils.getCurrentDate());
	}
}