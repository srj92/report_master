package co.md.sqs;

import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import co.md.ReportMasterAppTests;
import co.md.bean.ReportRequest;
import co.md.client.ReportGenerationSQSClient;
import co.md.enums.ReportTypeEnum;

@Ignore
public class SqsTest extends ReportMasterAppTests {

	@Autowired
	@Qualifier("reportGenerationSQSClient")
	private ReportGenerationSQSClient reportGenerationSQSClient;
	
	@Test
	public void testProducer() throws IOException {
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setCampaignId(12l);
		reportRequest.setReport(ReportTypeEnum.OS);
		
		reportGenerationSQSClient.sendMessage(reportRequest, "os_report_queue");
		
	}
}
