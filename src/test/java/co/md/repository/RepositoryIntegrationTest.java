package co.md.repository;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import co.md.ReportMasterAppTests;
import co.md.model.CmsCampaign;
import co.md.model.ReportStateHistory;
import co.md.util.DateTimeUtils;

@Ignore
public class RepositoryIntegrationTest extends ReportMasterAppTests {

	@Autowired
	private CmsCampaignRepository cmsCampaignRepository;
	
	@Autowired
	private ReportStateHistoryRepository reportStateHistoryRepository;
	
	@Before
	public void init() {
		Assert.assertNotNull(cmsCampaignRepository);
		Assert.assertNotNull(reportStateHistoryRepository);
	}
	
	@Test
	public void testFindAllValidCampaigns() {
		List<CmsCampaign> campaigns = cmsCampaignRepository.findAllValid(DateTimeUtils.getCurrentDate());
		Assert.assertTrue(campaigns.size() == 0);
	}
	
	@Test
	public void testInsertReportStateHistory() {
		
		ReportStateHistory reportStateHistory = new ReportStateHistory();
		reportStateHistory.setCampaignId(13L);
		reportStateHistory.setCreationDate(DateTimeUtils.getCurrentDate());
		reportStateHistory.setErrorMessage("test message");
		reportStateHistory.setReportType("os");
		reportStateHistory.setState(20L);
		
		reportStateHistory = reportStateHistoryRepository.save(reportStateHistory);
		
		Assert.assertNotNull(reportStateHistory.getId());
		
	}

	@Test
	public void testFetchReportStateHistory() {
		List<ReportStateHistory> reportStateHistoryList = (List<ReportStateHistory>) reportStateHistoryRepository.findAll();
		
		Assert.assertNotNull(reportStateHistoryList);
		Assert.assertTrue(!reportStateHistoryList.isEmpty());
	}
	
}
